# Duck pond simulator
Hi, welcome and thank you for taking this assignment. We set up this assignment to see how you think about 
object-oriented programming. For this assignment, the context will be a duck pond simulator. The duck pond simulator can
show a large variety of duck species swimming and making quacking sounds. Your assignment is to create these ducks. 

## Java
If you are doing this test in Java, please checkout the "java" branch. The README.md will contain all the information you 
need.

## PHP
If you are doing this test in PHP, please checkout the "php" branch. The README.md will contain all the information you
need.